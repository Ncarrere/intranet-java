package com.adistec.intranet.configuration;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.util.HashMap;

@Configuration
@EnableJpaRepositories(basePackages = "com.adistec.intranet.repository.sqlserver", entityManagerFactoryRef = "sqlserverEntityManager", transactionManagerRef = "sqlserverTransactionManager")
public class SqlServerConfiguration {

	@Bean
	public LocalContainerEntityManagerFactoryBean sqlserverEntityManager() throws NamingException {
		LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
		em.setDataSource(sqlserverDataSource());
		em.setPackagesToScan(new String[] { "com.adistec.intranet.entity.sqlserver" });
		HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
		em.setJpaVendorAdapter(vendorAdapter);
		HashMap<String, Object> properties = new HashMap<>();
		em.setJpaPropertyMap(properties);
		return em;
	}

	@Bean
	public PlatformTransactionManager sqlserverTransactionManager() throws NamingException {
		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(sqlserverEntityManager().getObject());
		return transactionManager;
	}

	@Bean(name = "sqlserverDataSource")
	@ConfigurationProperties(prefix="spring.sqlserver-data-source")
	public DataSource sqlserverDataSource() {
		return DataSourceBuilder.create().build();
	}
}
