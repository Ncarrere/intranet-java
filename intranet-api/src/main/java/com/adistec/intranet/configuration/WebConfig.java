package com.adistec.intranet.configuration;


import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import utils.SecurityContextUtil;

import java.util.Locale;

@Configuration
public class WebConfig implements WebMvcConfigurer {

	@Bean
	@Scope(value = WebApplicationContext.SCOPE_SESSION, proxyMode = ScopedProxyMode.TARGET_CLASS)
	public MessageSourceAccessor getMessageSource() {
		String userLanguage = SecurityContextUtil.getLocale();
		Locale locale = Locale.ENGLISH;
		if (userLanguage != null) {
			switch (userLanguage) {
			case "es":
				locale = new Locale("es");
				break;
			case "pt":
				locale = new Locale("pt");
				break;
			}
		}
		ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
		messageSource.setBasename("classpath:i18n/messages");
		messageSource.setDefaultEncoding("UTF-8");

		MessageSourceAccessor messageSourceAccessor = new MessageSourceAccessor(messageSource, locale);
		return messageSourceAccessor;
	}
}