package com.adistec.intranet.repository.mysql;

import com.adistec.intranet.entity.mysql.ContactFormLogMysql;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactFormLogMysqlRepository extends JpaRepository<ContactFormLogMysql, Long> {

	ContactFormLogMysql findById(Integer id);
}
