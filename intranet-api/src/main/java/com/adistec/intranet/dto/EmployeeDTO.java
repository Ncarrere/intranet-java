package com.adistec.intranet.dto;

import lombok.NoArgsConstructor;

import java.util.Date;

@NoArgsConstructor
public class EmployeeDTO {

    private Integer id;
    private String name;
    private String lastName;
    private String position;
    private String extendedTitle;
    private String email;
    private String phone;
    private String mobile;
    private String skype;
    private Integer branchOfficeId;
    private String areaId;
    private boolean showInMap;
    private Date birthday;
    private boolean director;
    private boolean enabled;
    private FileWrapper imgFile;

    public EmployeeDTO(Integer id, String name, String lastName, String position, String extendedTitle, String email, String phone, String mobile, String skype, Integer branchOfficeId, String areaId, boolean showInMap, Date birthday, boolean director, boolean enabled, FileWrapper imgFile) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.position = position;
        this.extendedTitle = extendedTitle;
        this.email = email;
        this.phone = phone;
        this.mobile = mobile;
        this.skype = skype;
        this.branchOfficeId = branchOfficeId;
        this.areaId = areaId;
        this.showInMap = showInMap;
        this.birthday = birthday;
        this.director = director;
        this.enabled = enabled;
        this.imgFile = imgFile;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public String getExtendedTitle() {
        return extendedTitle;
    }

    public void setExtendedTitle(String extendedTitle) {
        this.extendedTitle = extendedTitle;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public Integer getBranchOfficeId() {
        return branchOfficeId;
    }

    public void setBranchOfficeId(Integer branchOfficeId) {
        this.branchOfficeId = branchOfficeId;
    }

    public String getAreaId() {
        return areaId;
    }

    public void setAreaId(String areaId) {
        this.areaId = areaId;
    }

    public boolean isShowInMap() {
        return showInMap;
    }

    public void setShowInMap(boolean showInMap) {
        this.showInMap = showInMap;
    }

    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }

    public boolean isDirector() {
        return director;
    }

    public void setDirector(boolean director) {
        this.director = director;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public FileWrapper getImgFile() {
        return imgFile;
    }

    public void setImgFile(FileWrapper imgFile) {
        this.imgFile = imgFile;
    }
}
