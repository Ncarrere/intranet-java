package com.adistec.intranet.dto;

public class AppConfigurationDTO {

    private String apAddress;

    public AppConfigurationDTO(String apAddress) {
        this.apAddress = apAddress;
    }

    public String getApAddress() {
        return apAddress;
    }
}