package com.adistec.intranet.controller;

import com.adistec.intranet.dto.AppConfigurationDTO;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/app")
public class AppConfigurationController {

    @Value("${app.configuration.apAddress}")
    private String apAddress;

    @GetMapping("")
    public ResponseEntity<?> getAllappConfiguration() {
        return new ResponseEntity<>(new AppConfigurationDTO(apAddress), HttpStatus.OK);
    }
}