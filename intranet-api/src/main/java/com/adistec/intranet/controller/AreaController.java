package com.adistec.intranet.controller;

import com.adistec.intranet.configuration.MicroServicesConfiguration;
import org.keycloak.adapters.springsecurity.client.KeycloakRestTemplate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

@RestController
@RequestMapping(value = "/area")
public class AreaController {

	private static final Logger LOG = LoggerFactory.getLogger(AreaController.class);

	@Autowired
	@Qualifier("restTemplate")
	private RestTemplate restTemaplate;

	@Autowired
	private MicroServicesConfiguration microServicesConfiguration;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		try {
			LOG.info("Getting all areas ");
			return restTemaplate.exchange(microServicesConfiguration.getNetsuiteSupportUrl() + "/area",HttpMethod.GET,null,String.class);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			throw e;
		}
	}
}
