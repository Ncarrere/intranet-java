package com.adistec.intranet.controller;

import com.adistec.intranet.service.BranchOfficeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/office")
public class BranchOfficeController {

	private static final Logger LOG = LoggerFactory.getLogger(BranchOfficeController.class);

	@Autowired
	private BranchOfficeService branchOfficeService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		try {
			LOG.info("Getting all subsidiaries ");
			return new ResponseEntity<>(branchOfficeService.getAll(),HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			throw e;
		}
	}

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getById(@PathVariable Integer id) {
		try {
			LOG.info("Getting all subsidiaries ");
			return new ResponseEntity<>(branchOfficeService.getById(id),HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			throw e;
		}
	}
}
