package com.adistec.intranet.controller;

import com.adistec.intranet.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/employee")
public class EmployeeController {

	private static final Logger LOG = LoggerFactory.getLogger(EmployeeController.class);

	@Autowired
	private EmployeeService employeeService;

	@RequestMapping(value = "", method = RequestMethod.GET)
	public ResponseEntity<?> getAll() {
		try {
			LOG.info("Getting all employees ");
			return new ResponseEntity<>(employeeService.getAll(), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public ResponseEntity<?> getByid(@PathVariable Long id) {
		try {
			LOG.info("Getting employee with id " + id);
			return new ResponseEntity<>(employeeService.getById(id), HttpStatus.OK);
		} catch (Exception e) {
			LOG.error("Error inesperado", e);
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
