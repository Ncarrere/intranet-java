package com.adistec.intranet.service;

import com.adistec.intranet.dto.ContactFormLogDTO;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.net.URISyntaxException;

public interface ContactFormLogService {

	ContactFormLogDTO getById(Integer id);
	ContactFormLogDTO saveAndFlush(ContactFormLogDTO contactFormLogDTO, MultipartFile file) throws IOException, URISyntaxException;
}
