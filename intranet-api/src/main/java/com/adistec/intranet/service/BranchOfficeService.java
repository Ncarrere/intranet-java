package com.adistec.intranet.service;

import com.adistec.intranet.dto.BranchOfficeDTO;

import java.util.List;

public interface BranchOfficeService {
    List<BranchOfficeDTO> getAll();
    BranchOfficeDTO getById(Integer id) ;
}
