package com.adistec.intranet.service;

import com.adistec.intranet.dto.EmployeeDTO;

import java.util.List;

public interface EmployeeService {
    List<EmployeeDTO> getAll();
    EmployeeDTO getById(Long id) throws Exception;
}
