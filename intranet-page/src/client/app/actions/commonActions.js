import { axios } from './axiosWrapper';
import { 
	GET_EMPLOYEES_SUCCESS, 
	GET_EMPLOYEES_FAILED,
	GET_BRANCHOFFICES_SUCCESS,
	GET_BRANCHOFFICES_FAILED,
  GET_AREAS_SUCCESS,
  GET_AREAS_FAILED
} from '../actions/types';
import sortBy from 'lodash/sortBy';

import {increaseLoaderCount , decreaseLoaderCount } from './loaderActions';

const allowedLanguages = ['en','es','pt'];

export const getEmployees = () => {
    return dispatch => {
      increaseLoaderCount();
    	axios.get('/employee').then(response => {
          response.data = response.data.map(r => {
            r.fullName = `${r.name} ${r.lastName}`;
            return r;
          });
          decreaseLoaderCount();
        	dispatch({type: GET_EMPLOYEES_SUCCESS, payload: sortBy(response.data,['fullName'])})
       	}).catch(error => {
          decreaseLoaderCount();
       		dispatch({type: GET_EMPLOYEES_FAILED, payload: error.data})
       	});
    }
};

export const getBranchOffices = () => {
    return dispatch => {
      increaseLoaderCount();
    	axios.get('/office').then(response =>{ 
          decreaseLoaderCount();
        	dispatch({type: GET_BRANCHOFFICES_SUCCESS, payload: sortBy(response.data,['name'])})
       	}).catch(error => {
          decreaseLoaderCount();
       		dispatch({type: GET_BRANCHOFFICES_FAILED, payload: error.data})
       	});
    }
};

export const getAreas = () => {
    return dispatch => {
      increaseLoaderCount();
      axios.get('/area').then(response => {
          decreaseLoaderCount();
          dispatch({type: GET_AREAS_SUCCESS, payload: response.data})
        }).catch(error => {
          decreaseLoaderCount(); 
          dispatch({type: GET_AREAS_FAILED, payload: error.data})
        });
    }
};

export function getValidLanguage(lang) {
    return allowedLanguages.find(l => l == lang) ? lang : 'en'; 
}