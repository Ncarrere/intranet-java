/**
 * Created by gparis on 1/2/18.
 */
import React from 'react';
import { Field } from 'redux-form';
import GoogleCaptcha from 'react-google-recaptcha';

const Captcha = props => (
    <div>
        <GoogleCaptcha sitekey={props.siteKey} onChange={props.input.onChange} />
        {props.meta.touched && props.meta.invalid ? <error>
            {props.meta.error == 'Required' ? 'Please complete the Captcha' : 'Este campo es obligatorio'}
            </error> : null}
    </div>
);

const ReCaptcha = ({
    name,
    siteKey,
    validate
}) => (
    <div style={{width: '304px', margin: '0 auto'}}>
        <Field name={name}
               component={Captcha}
               siteKey={siteKey}
               validate={validate}/>
    </div>
);

export default ReCaptcha;
