import $ from 'jquery';
require('jquery.scrollto');

export const smoothScroll = {
	scrollTo: function (scrollTo,scrollSource,duration,settings) {
		let source = scrollSource || window;
		$(source).scrollTo(scrollTo,duration,settings);
	}
};