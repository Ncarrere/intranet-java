import React from 'react';
import { connect } from 'react-redux';
import Nav from '../components/Nav.jsx';

export default (ComposedComponent) => {
    class WithNavBar extends React.Component {
        
        renderNavElement = (introSectionText1,introSectionText2,link) => 
        (<div className="a-section_intro a-gradient_blue a-padding_30 a-padding--bottom_lg a-padding--top_0">
            <div className="container">
                <Nav authenticated={this.props.user} navigationButton={<li className="right">{link}</li>}/>
                <h2 className="center-align a-margin--bottom_0">{introSectionText1}</h2>
                <h4 className="center-align">{introSectionText2}</h4>
            </div>
        </div>)

        render() {
            return (
            	<ComposedComponent {...this.props} nav={this.renderNavElement.bind(this)}/>
            );
        }
    }

    return connect(
        state => {
            return {
              user: state.user.user
            }
        }, null
    )(WithNavBar);
};
