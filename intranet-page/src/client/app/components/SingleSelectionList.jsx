import React from 'react';
import { Field } from 'redux-form';

const RadioButtomGroup = (props) => {
    const change = (e) => {
        props.input.onChange(e.target.value.toString());
    }

    return (<ul className="single-selection-list a-theme_modern a-list_twocolumns">
        {props.list.map((element,index) =>
            <li key={index} data-state={element.name.toLowerCase().replace(/\s/g,'')}>
                <input type="radio" onChange={change} value={element.id.toString()}
                       id={`request${index}`} name={props.input.name}
                       checked={props.input.value == element.id.toString() ? true : false} disabled={props.disabled ? true : false}/>
                <label htmlFor={`request${index}`} >
                    <div className="item-icon">{element.icon}</div>
                    <div className="item-text">{element.name}
                        {element.description ? <span className="description">{element.description}</span> : null}
                    </div>
                </label>
            </li>
        )}
        {props.meta.submitFailed && props.meta.invalid ? <error>Debe elegir una opción</error> : null}
    </ul>)
}

const SingleSelectionList = ({list,name,validate,disabled}) => (
    <div>
        <Field component={RadioButtomGroup} validate={validate} list={list} name={name} disabled={disabled}/>
    </div> : null
)

export default SingleSelectionList;
