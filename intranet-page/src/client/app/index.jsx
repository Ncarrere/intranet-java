import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Route } from "react-router-dom";
import injectTapEventPlugin from 'react-tap-event-plugin';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import { Provider } from 'react-redux';
import reduxThunk from 'redux-thunk';
import { createStore, applyMiddleware, compose } from 'redux';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import reducers from './reducers/index';
import {getTranslations} from './config/translation.js'
import { cacheEnhancer } from 'redux-cache'
import { addLocaleData} from 'react-intl';
import es from 'react-intl/locale-data/es';
import pt from 'react-intl/locale-data/pt';
import DevTools from './config/devtools';
import {IntlProvider} from 'adistec-react-components';
import './assets/main.scss';
import MainContainer from './containers/MainContainer.jsx';
import RequireAuth from './containers/AuthWrapper.jsx';



injectTapEventPlugin({
    shouldRejectClick: () => document.body.querySelector('.Select-menu-outer')
});

const muiTheme = getMuiTheme({
    palette: {
        primary1Color: '#005fab',
        primary2Color: '#008fcb',
        accent1Color: '#005fab'
    },
    datePicker: {
        selectColor: '#005fab',
        color: '#005fab'
    },
    flatButton: {
        primaryTextColor: '#005fab'
    }
});
const translations = getTranslations();

const devTools = _ENV == 'dev' || _ENV == 'sit' ? <DevTools /> : null;

const createStoreWithMiddleware = compose(
    applyMiddleware(reduxThunk),cacheEnhancer()
   )(createStore);

const store = _ENV == 'dev' || _ENV == 'sit' ? createStoreWithMiddleware(reducers,DevTools.instrument()) : createStoreWithMiddleware(reducers);
addLocaleData([...es, ...pt]);
let intlProviderWrapper;

ReactDOM.render(
  <Provider store={store}>
    <IntlProvider ref={(component) => intlProviderWrapper = component} locale={'en'}  messages={translations}>
      <MuiThemeProvider muiTheme={muiTheme}>
            <div>
                {devTools}
                <BrowserRouter>
                    <Route path="" component={RequireAuth(MainContainer,true)} />
                </BrowserRouter>
            </div>
      </MuiThemeProvider>
    </IntlProvider>
  </Provider>
, document.getElementById('app'));

store.subscribe(
  () => {
    intlProviderWrapper.changeLocaleState('es');
  }
)
