import { AXIOS_REQUEST } from '../actions/types';

const INITIAL_STATE = { counter: {ready:0} };


export default function(state = INITIAL_STATE, action) {
    switch(action.type) {
        case AXIOS_REQUEST:
        	let newCounter = Object.assign({},state.counter);
            newCounter.ready = newCounter.ready + action.payload
            return {...state,
                counter: newCounter
            };
    }

    return state;
}

