import { USER_INFO } from '../actions/types';
import { getValidLanguage } from '../actions/commonActions';

const INITIAL_STATE = {name: '', lastName: '', email: '', locale: 'en', auth: false, roles:[],permissions:[],locale:getValidLanguage(navigator.language.split("-", 1)[0])};

export default (state = INITIAL_STATE, action) => {
 	switch(action.type) {
        case USER_INFO:
            return {...state, name: action.payload.firstName, lastName: action.payload.lastName,
            	email: action.payload.username, auth: true, roles: action.payload.roles,permissions: action.payload.permissions, locale: action.payload.locale};
    }

    return state;
}
