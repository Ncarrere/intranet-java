const mergeTranslations = (requireContext) => {
  const merged = {};
  requireContext.keys().forEach((key) => {
    Object.assign(merged, requireContext(key));
  });
  return merged;
};

export const getTranslations = () => {
  return translations;
}

const translations = {
  es: mergeTranslations(require.context('./i18n/es', false, /.json$/)),
  en: mergeTranslations(require.context('./i18n/en', false, /.json$/)),
  pt: mergeTranslations(require.context('./i18n/pt', false, /.json$/))
};
