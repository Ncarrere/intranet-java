import React from 'react';
import { connect } from 'react-redux';
import { Header , Loader , Footer } from 'adistec-react-components';
import { getAppConfiguration } from '../actions/appActions';
import {FormattedMessage} from 'react-intl';
import { axios } from '../actions/axiosWrapper';
import MenuItem from "material-ui/MenuItem";
import Divider from 'material-ui/Divider';
import logo from '../assets/img/adistec-intranet-white.png';
import {setUserInfo} from '../actions/userActions';
import {getKeycloak} from '../config/keycloak';

export default (ComposedComponent, requireAuth) => {
    class Authentication extends React.Component {
        constructor(props) {
            super(props);
            this.state = { keycloak: null, authenticated: false };
        }

        componentWillMount() {
            this.props.getAppConfiguration();
        }

        componentDidMount() {
            if(requireAuth && !getKeycloak().authenticated){
                getKeycloak().init({onLoad: 'login-required'}).success(authenticated => {
                  getKeycloak().loadUserInfo().success(userInfo => { 
                        this.props.setUserInfo(userInfo);
                        this.setState({keycloak:true,authenticated:true});
                  });
                })
            }
            else{
                  this.setState({keycloak:true,authenticated:true});
            }   
        }

        getMenuItems = () =>{
            return  [
                <MenuItem onTouchTap={() => {window.location.href = `${this.props.appConfig.apAddress}/myAccount`}}
                          leftIcon={<i style={{color: '#005fab'}}
                                       className="material-icons">person</i>}
                          primaryText={<FormattedMessage id='header.myAccount'/>}/>
                ,
                <Divider/>
                ,
                <MenuItem leftIcon={<i style={{color: '#005fab'}} className="material-icons">power_settings_new</i>} primaryText="Log out"   
                    onTouchTap={() => { getKeycloak().logout();}}
                />
            ]
        };

        render() {
            return (
                <div>
                    { this.state.keycloak && this.state.authenticated ?
                    <div id="main-wrapper" className="a-theme_modern">
                        <Header username={`${this.props.user.name} ${this.props.user.lastName}`}
                                userEmail={this.props.user.email}
                                menuItems={this.getMenuItems()}
                                apAddress={this.props.appConfig.apAddress}
                                logoSrc={logo}
                                companyName={this.props.user.companyName ? this.props.user.companyName : undefined}
                        />
                        {this.props.ready != 0 ? <Loader/> : null}
                        <main style={this.props.ready  != 0 ? {opacity:0.2} : null}>
                            <ComposedComponent {...this.props}/>
                        </main>
                        <Footer/>
                    </div> : null}
                </div>
            );
        }
    }

    return connect(
        state => {
            return {
              user: state.user,
              ready: state.loader.counter.ready,
              appConfig: state.appConfig.appConfig
            }
        }, { setUserInfo , getAppConfiguration }
    )(Authentication);
};
