import React from 'react';
import { Link } from 'react-router-dom'
import { reduxForm , Field, getFormValues , validate, getFormSyncErrors ,submit, touch,change,isValid,stopSubmit,reset} from 'redux-form';
import { connect } from 'react-redux';
import { axios } from '../actions/axiosWrapper';
import { Modal, UploadFile, TextField, Button, GeneralValidations, SelectField } from 'adistec-react-components';
import Nav from '../components/Nav.jsx';
import SingleSelectionList from '../components/SingleSelectionList.jsx';
import {FormattedMessage, injectIntl } from 'react-intl';
import { increaseLoaderCount, decreaseLoaderCount} from '../actions/loaderActions';
import {getUserInfo } from '../actions/userActions';
import BasesContent from '../containers/BasesContent.jsx';
import WithNavBar from '../components/WithNavBar.jsx';
import sortBy from 'lodash/sortBy';

class ReferidosForm extends React.Component{
    constructor(props) {
        super(props);
        this.state=
          {
            fileName: null,
            loader: false,
            open: false,
            areas: [],
            subsidiaries: [],
            basesModal: {
              open: false
            },
            requestResponse: {
              open: false,
              message: '',
              title: ''
            }
          }
    }

    componentWillMount(){
      this.getAllSubsidiaries();
      this.getAllAreas();
    }

    getAllSubsidiaries = () => {
      this.props.increaseLoaderCount();
      axios.get(`/office`).then(response => {
        this.setState({subsidiaries: sortBy(response.data,['name'])});
        this.props.decreaseLoaderCount();
      })
    };

    getAllAreas = () => {
      this.props.increaseLoaderCount();
      axios.get(`/area`).then(response => {
        this.setState({areas: response.data});
        this.props.decreaseLoaderCount();
      }).catch(error => {this.props.decreaseLoaderCount();})
    };

    saveReferral = formData => {
      this.props.increaseLoaderCount();
        axios.post(`/contactFormLog`, formData).then(response => {
          this.props.decreaseLoaderCount();
          this.setState({requestResponse : {open:true, message: "You have referred your friend successfuly!", title: "Success!"}});
          this.props.reset("ReferidosForm");
        }).catch(error => {this.props.decreaseLoaderCount();this.setState({requestResponse : {open:true, message: "There was a problem with your referral, try again later.", title: "Error!"}})});
    };

    handleSubmit = (data) => {
        let contactFormLog = Object.assign({},data);
        contactFormLog.countrySource = contactFormLog.country;
        let formData = new FormData();
        formData.append("contactFormLog",JSON.stringify(contactFormLog));
        if (contactFormLog.fileToUpload)
          formData.append("file", contactFormLog.fileToUpload.fileToUpload);
        this.saveReferral(formData);
    };

    handleClose = () => {
      this.setState({requestResponse:{open: false, message:'', title:''}});
      this.setState({basesModal:{open: false}});
    };

    handleOpenBases = () => {
        this.setState({basesModal:{open: true}});
    };
    getSubsidiaryOptions = () => {
      let options = [];
      this.state.subsidiaries.map((subsidiary, key) => options.push({label: subsidiary.name, value: subsidiary.name, key: key}));
      return options;
    };

    typeReferredList = () => {
      let array = [{id: "Referido",
                    name: "Referido",
                    icon: <i className="material-icons">comment</i>,
                    description: 'El candidato será un "referido" cuando un colaborador de Adistec no puede garantizar que el postulante cuenta con las habilidades y competencias para desempeñarse correctamente en la posición.'
                    },
                    {id: "Recomendado",
                        name: "Recomendado",
                        icon: <i className="material-icons">thumb_up</i>,
                        description:'El candidato será un "recomendado" cuando un colaborador de Adistec asegure que el postulante podrá desempeñarse correctamente en la posición.'
                    }];
      return array;
    };

    getAreasOptions = () => {
      let options = [];
      this.state.areas.map((area, key) => options.push({label: area.name, value: area.name, key: key}));
      return options;
    };

    modalItems = () => {
      let actions = [<Button label={"OK"} width='20' marginBottomTop="6" className="right" float='right' show onClick={this.handleClose.bind(this)} marginRight="10"/>];
      return actions;
    };

    render(){
      const { intl } = this.props;
            return (

                <div>
                    {this.props.nav(
                      <FormattedMessage id='referidosForm.introSectionText1'/>,
                      <FormattedMessage id='referidosForm.introSectionText2'/>,
                      <Link id="home" to="/" className="a-btn a-btn--border a-btn--pill a-btn_md a-btn_strong a-btn_white"><FormattedMessage id='topNav.employeeMap'/></Link>)}
                   
                    <div className="a-solid_lightblue a-section_main">
                        <div className="container a-move--moveup_md">
                          <form ref="form" onSubmit={this.props.handleSubmit(this.handleSubmit.bind(this))}>
                            <div className="col s12 m12">
                                <div className="a-card">
                                      <TextField name="searchComments"
                                                      size="12"
                                                      multiLine={true}
                                                      rows={1}
                                                      label={<FormattedMessage id='referidosForm.searchComments'/>}
                                                      className="adistec-textarea"
                                                      validate={GeneralValidations.required}
                                                      clearButton={true}
                                    />
                                </div>
                            </div>
                              <div className="row a--margin-top_15">
                                  <div className="col s6">
                                      <div className="a-card left a--margin-top_5 a--margin-bottom_15 a--padding-top_0 a--padding-bottom_0">
                                          <TextField name="firstName"
                                                     size="12"
                                                     label={<FormattedMessage id='referidosForm.name'/>}
                                                     validate={GeneralValidations.required}
                                                     clearButton={true}
                                          />
                                      </div>
                                  </div>
                                  <div className="col s6">
                                      <div className="a-card left a--margin-top_5 a--margin-bottom_15 a--padding-top_0 a--padding-bottom_0">
                                          <TextField name="lastName"
                                                     size="12"
                                                     label={<FormattedMessage id='referidosForm.lastName'/>}
                                                     validate={GeneralValidations.required}
                                                     clearButton={true}
                                          />
                                      </div>
                                  </div>
                                  <div className="col s6">
                                      <div className="a-card left a--margin-top_5 a--margin-bottom_15 a--padding-top_0 a--padding-bottom_0">
                                          <TextField name="email"
                                                     size="12"
                                                     label={<FormattedMessage id='referidosForm.email'/>}
                                                     validate={GeneralValidations.required}
                                                     clearButton={true}
                                          />
                                      </div>
                                  </div>
                                  <div className="col s6">
                                      <div className="a-card left a--margin-top_5 a--margin-bottom_15 a--padding-top_0 a--padding-bottom_0">
                                          <SelectField clearButton={true}
                                                       name="country"
                                                       size="12"
                                                       label={<FormattedMessage id='referidosForm.country'/>}
                                                       items={this.getSubsidiaryOptions()}
                                                       validate={GeneralValidations.required}
                                          />
                                      </div>
                                  </div>
                                  <div className="col s6">
                                      <div className="a-card left a--margin-top_5 a--margin-bottom_15 a--padding-top_0 a--padding-bottom_0">
                                          <TextField name="phone"
                                                     size="12"
                                                     label={<FormattedMessage id='referidosForm.phone'/>}
                                                     validate={GeneralValidations.required}
                                                     clearButton={true}
                                          />
                                      </div>
                                  </div>
                                  <div className="col s6">
                                      <div className="a-card left a--margin-top_5 a--margin-bottom_15 a--padding-top_0 a--padding-bottom_0">
                                          <SelectField clearButton={true}
                                                       name="position"
                                                       size="12"
                                                       label={<FormattedMessage id='referidosForm.position'/>}
                                                       items={this.getAreasOptions()}
                                                       validate={GeneralValidations.required}
                                          />
                                      </div>
                                  </div>
                              </div>
                              <div className="col s12 m12 a-margin--top_md">
                                  <h4 className="center-align">Incluya el Curriculum Vitae</h4>
                                  <div className="file-upload-container a-theme_modern">
                                      <UploadFile name="fileToUpload"
                                                         validate={GeneralValidations.required}
                                                         multiple={false}
                                                         size="12 m12 l4"
                                                         allowedFileTypes=".pdf, .zip, .jpg, .png, .doc, .docx, .xls, .xlsx"
                                                         maxSize={6291456}
                                                         label={<FormattedMessage id='referidosForm.fileToUploadLabel'/>}
                                                         icon={<i className="material-icons">backup</i>}
                                      />
                                  </div>
                              </div>
                              <div className="col s12 m12 a-margin--top_md">
                                <div className="col s12 m12 forceChildHeight140px">
                                    <SingleSelectionList list={this.typeReferredList()}
                                                         validate={GeneralValidations.required}
                                                         name="typeReferred" />
                                </div>
                              </div>
                              <div className="col s12 left" style={{marginTop:'15px'}}>
                                  <img className="referidos-logo" src="assets/img/referidos-logo.png"></img>
                                  <Button
                                      type="button"
                                      label={<FormattedMessage id='referidosForm.basesAndConditions'/>}
                                      onClick={this.handleOpenBases}
                                  />
                                </div>
                            <div className="bottom-buttons-container right a-margin--top_md">
                                <Button
                                  type="submit"
                                  label={<FormattedMessage id='referidosForm.refer'/>}
                                  primary={true}
                                  className="submit large"
                                  keyboardFocused={true}
                                  width={'auto'}
                                />
                            </div>
                        </form>
                        </div>
                    </div>
                    <Modal
                        open={this.state.requestResponse.open}
                        title={this.state.requestResponse.title}
                        content={<div style={{marginLeft: '10px', marginTop: '15px'}}>{this.state.requestResponse.message}</div>}
                        width="35%"
                        actions={this.modalItems()}
                        height="50%"
                        maxHeight="2%"
                    />
                    <Modal
                        open={this.state.basesModal.open}
                        title={<FormattedMessage id='referidosForm.basesAndConditions'/>}
                        content={<BasesContent />}
                        width="65%"
                        actions={this.modalItems()}
                    />
                </div>
            );

    }
}

ReferidosForm = reduxForm({
    form:'ReferidosForm',
    enableReinitialize: true,
    validate
})(ReferidosForm);

function mapStateToProps(state) {
    return {

        user: state.user
    };
}

ReferidosForm = connect(mapStateToProps,{
    reset,
    touch
})(ReferidosForm);

export default injectIntl(
  connect(mapStateToProps,{
      getUserInfo,
      increaseLoaderCount,
      decreaseLoaderCount,
      reset,
      touch
  })(WithNavBar(ReferidosForm)))
