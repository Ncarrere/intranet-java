import React from 'react';
import { connect } from 'react-redux';
import routes from '../constants/routes.jsx';
import { Switch, Route} from 'react-router-dom';

export default class MainContainer extends React.Component {
    constructor(props) {
        super(props);
    }

    render() {
		return (
	    	<div>
                <Switch>
                    {routes.map((r, index) => <Route key={index} exact path={r.path} component={r.component} />)}
                </Switch>    
	        </div>
        )
    }
}
