import React from 'react';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { Modal, TextField, Button, Filters , GeneralValidations} from 'adistec-react-components';
import Paper from 'material-ui/Paper';
import Nav from '../components/Nav.jsx';
import { FormattedMessage } from 'react-intl';
import { getEmployees , getBranchOffices , getAreas } from '../actions/commonActions';
import {
  ComposableMap,
  ZoomableGroup,
  Geographies,
  Geography,
  Markers,
  Marker
} from 'react-simple-maps';
import worldMap from '../assets/world-110m.json';
import keyBy from 'lodash/keyBy';
import merge from 'lodash/merge';
import { smoothScroll } from '../components/SmoothScroll';
import { coordinates } from '../constants/coordinates';
import WithNavBar from '../components/WithNavBar.jsx';

class MapaEmpleados extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            countries:[],
            detailsModal: {
                open: false,
                type: undefined
            },
            activeOffice:{},
            activeEmployee:undefined,
            filterdEmployees:[],
            activeSearch:undefined,
            activeFilter:undefined
        }
    }

    componentWillMount(){
        this.props.getEmployees();
        this.props.getBranchOffices();
        this.props.getAreas();
    }

    componentWillReceiveProps(nextProps){
        if(this.props.offices != nextProps.offices){
            let coordinatesByKey = keyBy(coordinates,'country');
            let offices = nextProps.offices.slice();
            offices.map(office => {
                if(coordinatesByKey[office.country]){
                    merge(office,coordinatesByKey[office.country]);
                }
            });
            this.setState({countries:offices});
        }
    }

    modalItems = () => {
        let actions = [<Button label={<FormattedMessage id='general.close'/>} width='20' marginBottomTop="6" className="right" float='right' show onClick={this.unmountModal.bind(this)} marginRight="10"/>];
        return actions;
    };

    getFiltersConfig = () => {
        return {
            searchInput:{
                'type': 'freeText',
                fuseOptions:{
                  shouldSort: true,
                  threshold: 0.1,
                  location: 0,
                  tokenize: true,
                  matchAllTokens: true,
                  distance: 100,
                  maxPatternLength: 50,
                  minMatchCharLength: 3,
                  keys: ['fullName', 'position']
                }
            }
        }
    }

    static contextTypes = {
      intl: React.PropTypes.object.isRequired
    }

    filtersFields = () => {
        return [
            <TextField autoComplete="off" key="1" name="searchInput" validate={[GeneralValidations.required, GeneralValidations.atLeastXCharacters(3)]} size="12" label={this.context.intl.formatMessage({id:'mapaEmpleados.searchInput'})}/>
        ];
    };

    afterApplyFilters = (filterdData,filters) => {
        if (filters.searchInput != undefined) {
          this.setState({detailsModal:{open:true,type:'search'},activeSearch:filters.searchInput,filterdEmployees:filterdData});
        }
    }

    setActiveOffice = (office) => {
        let officeEmployees = this.props.employees.filter(e => e.branchOfficeId == office.id);
        this.setState({activeOffice:office,detailsModal:{open: true,type:'details'},filterdEmployees:officeEmployees});
    }

    filterEmployees = (filterdEmployees,activeFilter) => {
        let modalBody = document.getElementsByClassName('employee-detail-modal-body');
        this.setState({filterdEmployees:filterdEmployees,activeFilter:activeFilter},function(){smoothScroll.scrollTo(this.employeesListElement,modalBody,1000)});
    }

    showEmployeeDetail = (employee,modelBodyElement) => {
        let modalBody = document.getElementsByClassName(modelBodyElement);
        if(employee.branchOfficeId == this.state.activeOffice){
            this.setState({activeEmployee:employee},function(){smoothScroll.scrollTo(this.activeEmployeeElement,modalBody,1000)});
        }
        else{
            let officeEmployees = this.props.offices.filter(e => e.id == employee.branchOfficeId)[0];
            this.setState({activeEmployee:employee,activeOffice:officeEmployees},function(){smoothScroll.scrollTo(this.activeEmployeeElement,modalBody,1000)});
        }

    }

    unmountModal = () => {
        this.setState({detailsModal:{open:false,type:undefined},activeOffice:{},activeEmployee:undefined,filterdEmployees:[],activeFilter:undefined});
    }

    employeeDetailModal = () => {
        return <Modal
            onRequestClose={this.unmountModal}
            bodyClass={'employee-detail-modal-body a--shadow-topbottom-inset'}
            open={this.state.detailsModal.open && this.state.detailsModal.type == 'details'}
            title={
                <div className="a--modal-header">
                    <strong className="office-title a--display-block"><span className="material-icons">place</span>
                        <span>{this.state.activeOffice.city ? `${this.state.activeOffice.city} | ${this.state.activeOffice.country}` : this.state.activeOffice.country}</span></strong>
                    {this.state.activeOffice.address ? <span className="office-address a--display-block">{this.state.activeOffice.address}</span> : null}
                    <span className="office-contact-info a--display-block">{this.state.activeOffice.address ? `Tel: ${ this.state.activeOffice.phone}` : null}
                    {this.state.activeOffice.email ? <strong> | {this.state.activeOffice.email}</strong> : null }</span>
                </div>
            }
            content={
                <div className="a-theme_modern" ref={(element => this.activeEmployeeElement = element)}>
                    {this.state.activeEmployee ?
                    <div className="a-gradient_blue">
                        <div className="container">
                            <div className="row mb0 selected-employee-details">
                                <div className="col s12 m4">
                                    <div className="a--profilepic">
                                        {this.state.activeEmployee.imgFile.imgUrl ?
                                            <div className="a--profilepic-image-circle" style={{backgroundImage:`url(${this.state.activeEmployee.imgFile.imgUrl})`}}></div>
                                            :
                                            <div className="a--profilepic-image-circle" style={{backgroundImage:`url(../assets/img/adistec-profilepic-notfound.png)`}}></div>
                                        }
                                    </div>
                                </div>
                                <div className="col s12 m8">
                                    <h1>{this.state.activeEmployee.fullName}</h1>
                                    <h2>{this.state.activeEmployee.position}</h2>
                                    {this.state.activeEmployee.phone ? <p><strong><FormattedMessage id='mapaEmpleados.phone'/></strong> {this.state.activeEmployee.phone}</p> : null}
                                    {this.state.activeEmployee.email ? <p><strong><FormattedMessage id='mapaEmpleados.email'/></strong> {this.state.activeEmployee.email}</p> : null}
                                    {this.state.activeEmployee.skype ? <p><strong><FormattedMessage id='mapaEmpleados.skype'/></strong> {this.state.activeEmployee.skype}</p> : null}
                                </div>
                            </div>
                        </div>
                    </div>: null}

                    <div className="container a--padding-topbottom-30">
                        <div className="row mb0">
                            <div className="col s2 m4 a--margin-bottom-20">
                                <Button
                                    className={!this.state.activeFilter ? 'active' : null}
                                    label={`${this.context.intl.formatMessage({id:'mapaEmpleados.all'})} (${this.props.employees.filter(e => e.branchOfficeId == this.state.activeOffice.id).length})`}
                                    onClick={this.filterEmployees.bind(this,this.props.employees.filter(e => e.branchOfficeId == this.state.activeOffice.id),undefined)}/>
                            </div>
                            {this.props.areas.map((area, key) => {
                                let employees = area.internalId == 'eje' ? this.props.employees.filter(e => (e.director || e.areaId == area.internalId) && e.branchOfficeId == this.state.activeOffice.id) //Si el area es 'Corporate' lista a todos los directores
                                                                         : this.props.employees.filter(e => e.areaId == area.internalId && e.branchOfficeId == this.state.activeOffice.id);
                                return (
                                    <div className="col s2 m4 a--margin-bottom-20" key={key}>
                                        <Button
                                            className={this.state.activeFilter == area.internalId ? 'active' : null}
                                            label={`${area.name} (${employees.length})`}
                                            onClick={this.filterEmployees.bind(this, employees ,area.internalId)}/>
                                    </div>
                                )
                                }
                            )}
                        </div>
                    </div>
                     <div className="a-solid_lightblue" ref={(element => this.employeesListElement = element)}>
                         <div className="container a--padding-topbottom-30">
                             <div className="row">
                                {this.state.filterdEmployees.map((employee, key) =>
                                    <div className="col s6 m3 l3" key={key}>
                                        <Paper className={`a--margin-bottom-20 employee-item ${this.state.activeEmployee && employee.id == this.state.activeEmployee.id ? 'active' : ''}`} onClick={this.showEmployeeDetail.bind(this,employee,'employee-detail-modal-body')}>
                                            <div className="a--profilepic">
                                                {employee.director ? <span className="a--profilepic-icon"><i className="material-icons">grade</i> <em>DIRECTORS</em></span> : null}
                                                {employee.position.indexOf("BPO") > -1 ? <span className="a--profilepic-icon green"><i className="ascii">&#9650;</i> <em>BPO-Quest</em></span> : null}
                                                {employee.imgFile.imgUrl ?
                                                    <div className="a--profilepic-image-circle" style={{backgroundImage:`url(${employee.imgFile.imgUrl})`}}></div>
                                                    :
                                                    <div className="a--profilepic-image-circle" style={{backgroundImage:`url(../assets/img/adistec-profilepic-notfound.png)`}}></div>
                                                }
                                                <span className="a--profilepic-name">{employee.fullName}</span>
                                            </div>
                                        </Paper>
                                    </div>
                                )}
                             </div>
                         </div>
                    </div>
                </div>
            }
            width="65%"
            actions={this.modalItems()}
        />
    }

    renderOfficeHeader = () => {
        return <div><strong className="office-title a--display-block"><span className="material-icons">place</span>
                        <span>{this.state.activeOffice.city ? `${this.state.activeOffice.city} | ${this.state.activeOffice.country}` : this.state.activeOffice.country}</span></strong>
                    {this.state.activeOffice.address ? <span className="office-address a--display-block">{this.state.activeOffice.address}</span> : null}
                    <span className="office-contact-info a--display-block">{this.state.activeOffice.address ? `Tel: ${ this.state.activeOffice.phone}` : null}
                    {this.state.activeOffice.email ? <strong> | {this.state.activeOffice.email}</strong> : null }</span></div>
    }

    employeeSearchModal = () => {
        return <Modal
            open={this.state.detailsModal.open && this.state.detailsModal.type == 'search'}
            bodyClass={'employee-search-modal-body a--shadow-topbottom-inset'}
            onRequestClose={this.unmountModal}
            title={
                
                <div className="a--modal-header">
                    <strong>{`${this.context.intl.formatMessage({id:'mapaEmpleados.searchInput'})}: ${this.state.activeSearch}`}</strong> 
                    { this.state.activeEmployee ? this.renderOfficeHeader() : null}
                </div>
            }
            content={
                <div className="a-theme_modern" ref={(element => this.activeEmployeeElement = element)}>
                    {this.state.activeEmployee ?
                    <div className="a-gradient_blue">
                        <div className="container">
                            <div className="row mb0 selected-employee-details">
                                <div className="col s12 m4">
                                    <div className="a--profilepic">
                                        {this.state.activeEmployee.imgFile.imgUrl ?
                                            <div className="a--profilepic-image-circle" style={{backgroundImage:`url(${this.state.activeEmployee.imgFile.imgUrl})`}}></div>
                                            :
                                            <div className="a--profilepic-image-circle" style={{backgroundImage:`url(../assets/img/adistec-profilepic-notfound.png)`}}></div>
                                        }
                                    </div>
                                </div>
                                <div className="col s12 m8">
                                    <h1>{this.state.activeEmployee.fullName}</h1>
                                    <h2>{this.state.activeEmployee.position}</h2>
                                    {this.state.activeEmployee.phone ? <p><strong><FormattedMessage id='mapaEmpleados.phone'/></strong> {this.state.activeEmployee.phone}</p> : null}
                                    {this.state.activeEmployee.email ? <p><strong><FormattedMessage id='mapaEmpleados.email'/></strong> {this.state.activeEmployee.email}</p> : null}
                                    {this.state.activeEmployee.skype ? <p><strong><FormattedMessage id='mapaEmpleados.skype'/></strong> {this.state.activeEmployee.skype}</p> : null}
                                </div>
                            </div>
                        </div>
                    </div>: null}
                     <div className="a-solid_lightblue">
                        <div className="container a--padding-topbottom-30">
                            <div className="row">
                            {this.state.filterdEmployees.length > 0 ?
                                this.state.filterdEmployees.map(employee =>
                                    <div className="col s6 m3 l3">
                                        <Paper className={`a--margin-bottom-20 employee-item ${this.state.activeEmployee && employee.id == this.state.activeEmployee.id ? 'active' : ''}`} onClick={this.showEmployeeDetail.bind(this,employee,'employee-search-modal-body')}>
                                            <div className="a--profilepic">
                                                {employee.director ? <span className="a--profilepic-icon"><i className="material-icons">grade</i> <em>DIRECTORS</em></span> : null}
                                                {employee.position.indexOf("BPO") > -1 ? <span className="a--profilepic-icon green"><i className="ascii">&#9650;</i> <em>BPO-Quest</em></span> : null}
                                                {employee.imgFile.imgUrl ?
                                                    <div className="a--profilepic-image-circle" style={{backgroundImage:`url(${employee.imgFile.imgUrl})`}}></div>
                                                    :
                                                    <div className="a--profilepic-image-circle" style={{backgroundImage:`url(../assets/img/adistec-profilepic-notfound.png)`}}></div>
                                                }
                                                <span className="a--profilepic-name">{employee.fullName}</span>
                                            </div>
                                        </Paper>
                                    </div>
                                )
                                : <FormattedMessage id='mapaEmpleados.resultsNotFound'/>}
                            </div>
                        </div>
                    </div>
                </div>
            }
            width="65%"
            actions={this.modalItems()}
        />
    }

    render() {
		return (
            <div>
                {this.props.nav(
                    <FormattedMessage id='mapaEmpleados.introSectionText1'/>,
                    <FormattedMessage id='mapaEmpleados.introSectionText2'/>,
                    <Link id="home" to="/referidos" className="a-btn a-btn--border a-btn--pill a-btn_md a-btn_strong a-btn_white"><FormattedMessage id='topNav.referafriend'/></Link>)}
                <div className="a-solid_lightblue a-section_main">
                    <div className="container a-move--moveup_md">
                        <div className="row">
                            <Filters
                                fields={this.filtersFields()}
                                disableAutocomplete
                                afterApplyFilter={this.afterApplyFilters.bind(this)}
                                data={this.props.employees}
                                viewMoreButton={false}
                                hidden={false}
                                containerClass="col s12" open={true}
                                config={this.getFiltersConfig()}
                            />
                            <div className="col s12 center">
                                <div className="a-solid_darkblue a--borderradius-5 a--margin-top-30">
                                    <ComposableMap
                                        style={{
                                            width: "800px",
                                            height: "850px"
                                        }}
                                        projectionConfig={{
                                            scale: 600
                                        }}>
                                        <ZoomableGroup center={[-64.381559100000004,-14.6036844]}
                                                       zoom={1}
                                                       disablePanning
                                        >
                                            <Geographies geography={worldMap}>
                                                {(geographies, projection) => geographies.filter(g => g.properties.REGION_UN == 'Americas' && g.properties.NAME != "Canada").map((geography, key) => (
                                                    <Geography
                                                        key={ key }
                                                        geography={ geography }
                                                        projection={ projection }
                                                        style={{
                                                            default: {
                                                                fill: "#00365c",
                                                                stroke: "#005490",
                                                                strokeWidth: 2
                                                            },
                                                            hover: {
                                                                fill: "#00365c",
                                                                stroke: "#005490",
                                                                strokeWidth: 2
                                                            },
                                                            pressed: {
                                                                fill: "#00365c",
                                                                stroke: "#005490",
                                                                strokeWidth: 2
                                                            }
                                                        }}
                                                    />
                                                ))}
                                            </Geographies>
                                            <Markers>
                                                {this.state.countries.map((country, key) => {
                                                    if(country.coordinates){
                                                        return <Marker
                                                            key={key}
                                                            marker={{ coordinates: [...country.coordinates] }}
                                                            style={{
                                                                default: { fill: "#FFFFFF" },
                                                                hover:   { fill: "#EEEEEE" },
                                                                pressed: { fill: "#DDDDDD" },
                                                            }}
                                                            onClick={this.setActiveOffice.bind(this,country)}>
                                                            <g>
                                                                <path transform={"translate(-12, -24)"}
                                                                      d={"M20,9c0,4.9-8,13-8,13S4,13.9,4,9c0-5.1,4.1-8,8-8S20,3.9,20,9z"}
                                                                      className="map-marker-shadow"
                                                                />
                                                            </g>


                                                            {country.rtl ?
                                                                <text textAnchor={"end"} fontSize={16} x={-12} y={-7}>{`${this.props.employees.filter(e => e.branchOfficeId == country.id).length} | ${country.country}`}</text>
                                                                :
                                                                <text textAnchor={"start"} fontSize={16} x={12} y={-7}>{`${country.country} | ${this.props.employees.filter(e => e.branchOfficeId == country.id).length}`}</text>
                                                            }
                                                        </Marker>
                                                    }
                                                })}
                                            </Markers>
                                        </ZoomableGroup>
                                    </ComposableMap>
                                </div>
                            </div>
                        </div>
                        {this.employeeDetailModal()}
                        {this.employeeSearchModal()}
                    </div>
                </div>
            </div>
        )
    }
}


export default connect(
    state => {
        return {
            employees: state.commons.employees,
            offices: state.commons.offices,
            areas: state.commons.areas
        }
    },{getEmployees , getBranchOffices , getAreas}
)(WithNavBar(MapaEmpleados));
